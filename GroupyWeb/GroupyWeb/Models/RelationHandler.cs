﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public static class RelationHandler
    {
        public static void NewGroupRelation(List<PersonRelation> relations, Person p1, Person p2)
        {
            RelationOf2People(relations, p1, p2).AddTimesWorked();
        }

        public static int TimesWorkedTogether(List<PersonRelation> relations, Person p1, Person p2)
        {
            return RelationOf2People(relations, p1, p2).TimesWorked;
        }

        public static int MaxTimesWorkedTogether(List<PersonRelation> relations, List<Person> persons, Person p1)
        {
            int maxTimes = int.MinValue;

            for (int i = 0; i < persons.Count; i++)
            {
                maxTimes = Math.Max(maxTimes, RelationOf2People(relations, p1, persons[i]).TimesWorked);
            }
            return maxTimes;
        }
        public static  int MaxTimesWorkedTogether(List<PersonRelation> relations, Person p1, Person[] groupmembers)
        {
            int maxTimes = int.MinValue;
            for (int i = 0; i < groupmembers.Length; i++)
            {
                if (p1 != groupmembers[i])
                {
                    maxTimes = Math.Max(maxTimes, RelationOf2People(relations,p1, groupmembers[i]).TimesWorked);
                }
            }
            return maxTimes;
        }

        public static PersonRelation RelationOf2People(List<PersonRelation> relations, Person p1, Person p2)
        {
            PersonRelation relation = relations.Find(r => (r.P1 == p1 && r.P2 == p2) || (r.P2 == p1 && r.P1 == p2));
            if (relation==null)
            {
                //add relation?
            }
            return relation;
        }
    }
}
