﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public class GroupyUser : IdentityUser
    {
        public GroupyUser()
        {
            Settings = new HashSet<Setting>();
        }

        public ICollection<Setting> Settings { get; set; }
    }
}