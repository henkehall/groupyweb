﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroupyLogic;

namespace GroupyWeb.Models
{
    public static class GroupMaker
    {
        public static GroupSet AssignToGroups(Person[] people, IList<PersonRelation> relations, int numberOfGroups, out List<MembersInGroups> membersInGroups)
        {
            Dictionary<int, int> peopleIndexToId = new Dictionary<int, int>();
            Dictionary<int, int> peopleIdToIndex = new Dictionary<int, int>();
            for (int i = 0; i < people.Length; i++)
            {
                peopleIndexToId.Add(i, people[i].Id);
                peopleIdToIndex.Add(people[i].Id, i);
            }

            LogicGroupSet logicGroupSet = new LogicGroupSet();
            LogicRelationhandler logicRelations = PersonRelationsToLRelationHandler(relations, peopleIdToIndex);
            logicGroupSet.RandomizeGroupByNumber(numberOfGroups, people.Length, logicRelations);
            //logicRelations = PersonRelationsToLRelationHandler(relations, peopleIdToIndex);

            GroupSet groupSet = LGroupSetToGroupSet(logicGroupSet, people, out membersInGroups);
            return groupSet;
            //LRelationHandlerToPersonRelations(relations, logicRelations, peopleIndexToId);
        }

        private static GroupSet LGroupSetToGroupSet(LogicGroupSet logicGroupSet, Person[] people, out List<MembersInGroups> membersInGroups)
        {
            GroupSet groupSet = new GroupSet();

            LogicGroup[] logicGroups= logicGroupSet.Groups.ToArray();
            Group[] groups = new Group[logicGroups.Length];
            membersInGroups = new List<MembersInGroups>();
            for (int i = 0; i < logicGroups.Length; i++)
            {
                groups[i] = new Group();
                int[] memberArray = logicGroups[i].MembersArray;
                for (int j = 0; j < memberArray.Length; j++)
                {
                    membersInGroups.Add(new MembersInGroups());
                    membersInGroups.Last().Member = people[memberArray[j]];
                    membersInGroups.Last().Group = groups[i];
                }
                groupSet.Groups.Add(groups[i]);
            }
            return groupSet;
        }

        private static LogicRelationhandler PersonRelationsToLRelationHandler(IList<PersonRelation> relations, Dictionary<int, int> peopleIdToIndex)
        {
            LogicRelationhandler logicRelations = new LogicRelationhandler(peopleIdToIndex.Count);
            for (int i = 0; i < relations.Count; i++)
            {
                logicRelations.SetRelation(peopleIdToIndex[relations[i].P1id],
                                           peopleIdToIndex[relations[i].P2id],
                                           relations[i].TimesWorked,
                                           relations[i].Avoid);
            }
            return logicRelations;
        }

        private static IList<PersonRelation> LRelationHandlerToPersonRelations(IList<PersonRelation> relations,
            LogicRelationhandler logicRelations, Dictionary<int, int> peopleIndexToId)
        {
            LogicRelation[][] relationMatrix = logicRelations.RelationMatrix;
            for (int i = 0; i < relationMatrix.Length; i++)
            {
                for (int j = 0; j < relationMatrix[i].Length; j++)
                {
                    try
                    {
                        relations.Where(r => (r.P1.Id == peopleIndexToId[i] && r.P2.Id == peopleIndexToId[j]) ||
                                             (r.P1.Id == peopleIndexToId[j] && r.P2.Id == peopleIndexToId[i]))
                                             .FirstOrDefault().TimesWorked = relationMatrix[i][j].TimesWorked;
                    }
                    catch (NullReferenceException)
                    {
                        throw; //probably: relation non existant in relations.
                    }
                }
            }
            return new List<PersonRelation>();
        }
    }
    //when doing with half assigned groups:
    //make dictionary with <personIndex,PreselectedGroup>
}
