﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GroupyWeb.Models
{
    public class MembersInGroups
    {
        [Key, Column(Order = 0)]
        public int GroupId { get; set; }
        [Key, Column(Order = 1)]
        public int MemberId { get; set; }

        public Group Group { get; set; }
        public Person Member { get; set; }
    }
}
