﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public class Setting
    {
        public Setting()
        {
            Groupsets = new HashSet<GroupSet>();
            Persons = new HashSet<Person>();
        }

        public int Id { get; set; }
        public String UserId { get; set; }

        public GroupyUser GroupyUser { get; set; }
        public ICollection<GroupSet> Groupsets { get; set; }
        public ICollection<Person> Persons { get; set; }
    }
}
