﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    //[Table("PersonRelation")]
    public class PersonRelation
    {
        [Required]
        public int P1id { get; set; }
        [Required]
        public int P2id { get; set; }
        public bool Avoid { get; set; }
        public int TimesWorked { get; set; }

        public Person P1 { get; set; }
        public Person P2 { get; set; }

        int timesWorked;
        bool avoid;

        public PersonRelation()
        {
            timesWorked = 0;
            avoid = false;
        }

        public PersonRelation(Person p1, Person p2)
        {
            P1 = p1;
            P2 = p2;
            timesWorked = 0;
            avoid = false;
        }

        public void AddTimesWorked()
        {
            timesWorked++;
        }

        //public int TimesWorked
        //{
        //    get
        //    {
        //        return timesWorked;
        //    }
        //    set
        //    {
        //        timesWorked = value;
        //    }
        //}

        //public bool Avoid
        //{
        //    get
        //    {
        //        return avoid;
        //    }
        //    set
        //    {
        //        avoid = value;
        //    }
        //}
    }
}