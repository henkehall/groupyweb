﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public class Person
    {
        public Person()
        {
            MembersInGroups = new HashSet<MembersInGroups>();
            PersonRelationsP1 = new HashSet<PersonRelation>();
            PersonRelationsP2 = new HashSet<PersonRelation>();
            Active = true;
        }

        public int Id { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        public int SettingId { get; set; }

        public Setting Setting { get; set; }
        public ICollection<MembersInGroups> MembersInGroups { get; set; }
        public ICollection<PersonRelation> PersonRelationsP1 { get; set; }
        public ICollection<PersonRelation> PersonRelationsP2 { get; set; }
    }
}