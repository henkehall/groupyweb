﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public class GroupSet
    {
        public GroupSet()
        {
            Groups = new HashSet<Group>();
        }

        public int Id { get; set; }
        public int? SettingId { get; set; }

        public Setting Setting { get; set; }
        public ICollection<Group> Groups { get; set; }

        static int callCounter = 0;
        private List<Group> groups = new List<Group>();
        public int id;

        //public void RandomizeGroupsBySize(int peopleInGroup, int totalPersons)
        //{
        //    // 
        //}

        public void RandomizeGroupByNumber(List<PersonRelation> relations, Person[] persons, int numberOfGroups)
        {
            //create new groups
            for (int i = 0; i < numberOfGroups; i++)
            {
                Group newGroup = new Group();
                groups.Add(newGroup);
            }

            //PopulateGroupFetchBruteforce2(persons, relations);

            //RegisterMemberRelations(relations);
        }

        #region current algorithm
        //private void PopulateGroupFetchBruteforce2(Person[] persons, List<PersonRelation> relations)
        //{
        //    callCounter++;
        //    List<Group>[] groupSetAlternatives = new List<Group>[5];
        //    double[] groupSetAltPointsStdDev = new double[groupSetAlternatives.Length];
        //    int[][] groupSetAltPoints = new int[groupSetAlternatives.Length][];
        //    int numberOfAlternatives = 0;
        //    Random rnd = new Random();

        //    for (int altNbr = 0; altNbr < groupSetAlternatives.Length; altNbr++)
        //    {
        //        Console.ForegroundColor = ConsoleColor.DarkRed;
        //        Console.WriteLine("new alt");
        //        Console.ResetColor();
        //        //create new groups
        //        groupSetAlternatives[altNbr] = new List<Group>();
        //        for (int i = 0; i < groups.Count; i++)
        //        {
        //            groupSetAlternatives[altNbr].Add(new Group());
        //        }
        //        List<Person> PeopleToAdd = persons.ToList();

        //        int groupMaxSize = persons.Length / groupSetAlternatives[altNbr].Count;
        //        int slotsForGroupsWithMaxSize = groupSetAlternatives[altNbr].Count;
        //        if (persons.Length % groupSetAlternatives[altNbr].Count > 0)
        //        {
        //            groupMaxSize += 1;
        //            slotsForGroupsWithMaxSize = persons.Length % groupSetAlternatives[altNbr].Count;
        //        }

        //        int randomPerson = rnd.Next(0, PeopleToAdd.Count - 1);

        //        //add first person to group one to get started
        //        //groupSetAlternatives[altNbr][0].AddMember(PeopleToAdd[randomPerson]);
        //        MembersInGroups mem = new MembersInGroups();
        //        mem.Member = PeopleToAdd[randomPerson];
        //        mem.Group = groupSetAlternatives[altNbr][0];
        //        groupSetAlternatives[altNbr][0].MembersInGroups.Add(mem);
        //            AddMember(PeopleToAdd[randomPerson]);

        //        //remove the person that was just added to groups
        //        PeopleToAdd.RemoveAt(randomPerson);

        //        while (PeopleToAdd.Count > 0)
        //        {
        //            for (int i = 0; i < groupSetAlternatives[altNbr].Count; i++)
        //            {
        //                bool added = false;
        //                if ((groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize && slotsForGroupsWithMaxSize > 0)
        //                   || groupSetAlternatives[altNbr][i].MembersArray.Length < groupMaxSize - 1) //if group has at lest one space left
        //                {
        //                    //this block is to find the person that has worked the leat times with current members
        //                    Person[] currentMembers = new Person[groupSetAlternatives[altNbr][i].MembersArray.Length];
        //                    for (int j = 0; j < currentMembers.Length; j++)
        //                    {
        //                        currentMembers[j] = groupSetAlternatives[altNbr][i].MembersArray[j];
        //                    }
        //                    int minPoints = int.MaxValue;
        //                    int indexOfPersonLeastPoints = 0;
        //                    //Console.WriteLine("zeroing minPoints");
        //                    for (int j = 0; j < PeopleToAdd.Count; j++)
        //                    {
        //                        int points = 0;

        //                        for (int k = 0; k < currentMembers.Length; k++)
        //                        {
        //                            points += RelationHandler.TimesWorkedTogether(relations,
        //                                PeopleToAdd[j], currentMembers[k]);
        //                            //Console.WriteLine($"comparing p {peopleList[j].Item1} and " +
        //                            //    $"p {currentMembers[k]}. the points are {relations.TimesWorkedTogether(peopleList[j].Item1, currentMembers[k])}, totpoints: {points}");
        //                        }
        //                        if (points < minPoints)
        //                        {
        //                            minPoints = points;
        //                            indexOfPersonLeastPoints = j;
        //                            //Console.WriteLine($"new lowest for p {peopleList[j].Item1}");
        //                        }
        //                    }

        //                    groupSetAlternatives[altNbr][i].AddMember(PeopleToAdd[indexOfPersonLeastPoints]);
        //                    PeopleToAdd.RemoveAt(indexOfPersonLeastPoints);

        //                    //remove one slot from groups able to have max number of members if max members in group
        //                    if (groupSetAlternatives[altNbr][i].NumberOfMembers == groupMaxSize)
        //                    {
        //                        slotsForGroupsWithMaxSize--;
        //                    }

        //                    added = true;
        //                }

        //                if (added)
        //                    break;
        //            }
        //        }
        //        //Console.WriteLine(callCounter+ " hello");

        //        //Console.WriteLine($"optimizing {altNbr}");
        //        for (int i = 0; true; i++)
        //        {
        //            //if (i % 10 == 0)
        //            //{
        //            //    Console.WriteLine(i);
        //            //}

        //            groupSetAlternatives[altNbr] = OptimizeGroupSet(groupSetAlternatives[altNbr], relations, persons, out bool changed);
        //            groupSetAltPointsStdDev[altNbr] = Mathematics.StandardDeviation(GroupSetPoints2(groupSetAlternatives[altNbr], relations));
        //            if (groupSetAltPointsStdDev[altNbr] == 0 || !changed)
        //            {
        //                break;
        //            }
        //            //break;
        //        }

        //        groupSetAltPoints[altNbr] = GroupSetPoints2(groupSetAlternatives[altNbr], relations);
        //        groupSetAltPointsStdDev[altNbr] = Mathematics.StandardDeviation(groupSetAltPoints[altNbr]);

        //        numberOfAlternatives++;
        //        //Console.WriteLine("This standard Dev: " + groupSetAltPointsStdDev[altNbr]);
        //        if (groupSetAltPoints[altNbr].Max() - groupSetAltPoints[altNbr].Min() < 2)
        //        {
        //            break;
        //        }
        //    }
        //    Console.WriteLine("get best alt");
        //    double bestGroupSet = int.MaxValue;
        //    for (int i = 0; i < numberOfAlternatives; i++)
        //    {
        //        if (groupSetAltPointsStdDev[i] < bestGroupSet)
        //        {
        //            bestGroupSet = groupSetAltPointsStdDev[i];
        //            groups = groupSetAlternatives[i];
        //            Console.WriteLine($"number {i}, standard Dev: " + groupSetAltPointsStdDev[i]);
        //        }
        //    }
        //    //get the n different alternatives and find the best one and set it to groups
        //}
        #endregion

        //private static int[] GroupSetPoints(List<Group> groupList, List<PersonRelation> relations)
        //{
        //    int numberOfRelations = 0;
        //    //calculating the number of relations
        //    for (int i = 0; i < groupList.Count; i++)//loop the groups
        //    {
        //        int n = groupList[i].MembersArray.Count();
        //        numberOfRelations += n * (n - 1) / 2;
        //    }

        //    //Console.WriteLine($"relationscounter = {numberOfRelations}");

        //    //get the standard deviation of the new number of times worked together for everyone in the groupset

        //    int[] groupSetPoints = new int[numberOfRelations];
        //    int relationCount = 0;
        //    for (int i = 0; i < groupList.Count; i++)//loop the groups
        //    {
        //        for (int j = 0; j < groupList[i].MembersArray.Count(); j++)//loop the members
        //        {
        //            for (int k = 0; k < j; k++)
        //            {
        //                if (j != k)
        //                {
        //                    groupSetPoints[relationCount] = RelationHandler.TimesWorkedTogether(relations, groupList[i].MembersArray[j], groupList[i].MembersArray[k]) + 1;
        //                    relationCount++;
        //                }
        //            }
        //        }
        //    }
        //    return groupSetPoints;
        //}

        //private static int[] GroupSetPoints2(List<Group> groupList, List<PersonRelation> relations)
        //{
        //    List<int> groupSetPoints = new List<int>();

        //    for (int i = 0; i < relations.Count; i++)
        //    {
        //        if (relations[i].P1.Active && relations[i].P2.Active) // if both are active
        //        {
        //            bool inSameGroup = false;
        //            for (int j = 0; j < groupList.Count; j++)
        //            {
        //                if (groupList[j].MembersArray.Contains(relations[i].P1) &&
        //                    groupList[j].MembersArray.Contains(relations[i].P2)) //if both are in same group
        //                {
        //                    groupSetPoints.Add(relations[i].TimesWorked + 1);
        //                    inSameGroup = true;
        //                }
        //            }
        //            if (!inSameGroup)
        //            {
        //                groupSetPoints.Add(relations[i].TimesWorked);
        //            }
        //        }
        //    }
        //    return groupSetPoints.ToArray();
        //}



        //public static List<Group> OptimizeGroupSet(List<Group> groupList, List<PersonRelation> relations, Person[] persons, out bool changed)
        //{
        //    var groupSetPoints = GroupSetPoints2(groupList, relations);
        //    double standardDeviation = Mathematics.StandardDeviation(groupSetPoints);
        //    int medianWorkedWith = Mathematics.Median(groupSetPoints);
        //    int leastDiffTimesWorked = groupSetPoints.Max() - groupSetPoints.Min();
        //    int[] newGroupSetPoints;
        //    Console.WriteLine("optimizing...");
        //    Console.WriteLine($"orig sd:{standardDeviation}");

        //    List<Person> possibleSwitchers = PossibleSwitchers(groupList, medianWorkedWith, relations);
        //    //List<Person> possibleSwitchers = persons.ToList();

        //    bool done = false;
        //    changed = false;
        //    for (int i = 0; i < possibleSwitchers.Count && !done; i++)
        //    {
        //        for (int j = 0; j < persons.Length && !done; j++)
        //        {
        //            if (possibleSwitchers[i] != persons[j])
        //            {
        //                groupList = SwitchMembers(groupList, possibleSwitchers[i], persons[j]);
        //                //foreach (var item in GroupSetPoints2(groupList, relation))
        //                //{
        //                //    Console.Write(item+", ");
        //                //}
        //                //Console.WriteLine();
        //                newGroupSetPoints = GroupSetPoints2(groupList, relations);
        //                double newStdDev = Mathematics.StandardDeviation(newGroupSetPoints);
        //                if (newStdDev < standardDeviation )//&& 
        //                   //(newGroupSetPoints.Max() - newGroupSetPoints.Min()) <= leastDiffTimesWorked)
        //                {
        //                    standardDeviation = newStdDev;
        //                    leastDiffTimesWorked = newGroupSetPoints.Max() - newGroupSetPoints.Min();
        //                    Console.WriteLine($"new internal sd:{newStdDev}, {PrintNumberOfEachInArray(newGroupSetPoints)}");
        //                    changed = true;
        //                }
        //                else if (newStdDev == standardDeviation)
        //                {

        //                }
        //                else
        //                {
        //                    groupList = SwitchMembers(groupList, persons[j], possibleSwitchers[i]);
        //                }
        //                if (newStdDev==0)
        //                {
        //                    done = true;
        //                }
        //            }
        //        }
        //    }
        //    return groupList;
        //}

        //private static List<Person> PossibleSwitchers(List<Group> groupList, int medianWorkedWith, List<PersonRelation> relations)
        //{
        //    List<Person> possibleSwitchers = new List<Person>();
        //    for (int i = 0; i < groupList.Count; i++) //loop through groups
        //    {
        //        for (int j = 0; j < groupList[i].MembersArray.Length; j++) //loop through members in group
        //        {
        //            if (RelationHandler.MaxTimesWorkedTogether(relations, groupList[i].MembersArray[j], groupList[i].MembersArray) + 1 > medianWorkedWith)
        //            {
        //                possibleSwitchers.Add(groupList[i].MembersArray[j]);
        //            }
        //        }
        //    }

        //    //Console.WriteLine("possible switchers:");
        //    //foreach (var person in possibleSwitchers) //debug print
        //    //{
        //    //    Console.WriteLine(person.Id);
        //    //}
        //    return possibleSwitchers;
        //}

        //static List<Group> SwitchMembers(List<Group> groupList,Person p1,Person p2)
        //{
        //    //Console.WriteLine("new switch");
        //    for (int i = 0; i < groupList.Count; i++) //loop through groups
        //    {
        //        //Console.WriteLine($"group {i}");
        //        if (groupList[i].MembersArray.Contains(p1) && groupList[i].MembersArray.Contains(p2))
        //        {
        //            //nothing
        //        }
        //        else if (groupList[i].MembersArray.Contains(p1))
        //        {
        //            //Console.WriteLine($"removing {p1.Id}, adding {p2.Id}");
        //            groupList[i].RemoveMember(p1);
        //            groupList[i].AddMember(p2);
        //        }
        //        else if (groupList[i].MembersArray.Contains(p2))
        //        {
        //            //Console.WriteLine($"removing {p2.Id}, adding {p1.Id}");
        //            groupList[i].RemoveMember(p2);
        //            groupList[i].AddMember(p1);
        //        }
        //    }
        //    return groupList;
        //}

        //private void RegisterMemberRelations(List<PersonRelation> relations)
        //{
        //    //register relations between groupmembers
        //    foreach (var group in groups)
        //    {
        //        for (int i = 0; i < group.NumberOfMembers; i++)
        //        {
        //            for (int j = 0; j < i; j++)
        //            {
        //                RelationHandler.NewGroupRelation(relations, group.MembersArray[i], group.MembersArray[j]);
        //            }
        //        }
        //    }
        //}

        //public string ShowGroups()
        //{
        //    string output = "";
        //    for (int i = 0; i < groups.Count; i++)
        //    {
        //        output += "Group " + i + "    " + groups[i].NumberOfMembers + " members.";

        //        Person[] groupMembers = groups[i].MembersArray;
        //        foreach (var groupMember in groupMembers)
        //        {
        //            output += "\n"+groupMember.Name + " " + groupMember.Id;
        //        }
        //        output += "\n===================\n";
        //    }
        //    return output;
        //}

        //for debugging
        static string PrintNumberOfEachInArray(int[] array)
        {
            Dictionary<int, int> NumberOfEach = new Dictionary<int, int>();
            for (int i = 0; i < array.Length; i++)
            {
                if (NumberOfEach.ContainsKey(array[i]))
                {
                    NumberOfEach[array[i]]++;
                }
                else
                {
                    NumberOfEach.Add(array[i], 1);
                }
                
            }
            var orderedNumberOfEach = NumberOfEach.OrderBy(x => x.Value);
            string output = "";
            foreach (var each in orderedNumberOfEach)
            {
                output += $"{each.Value} of {each.Key}, ";
            }
            return output;
        }
    }
}
