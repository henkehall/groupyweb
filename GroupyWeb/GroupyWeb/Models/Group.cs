﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyWeb.Models
{
    public class Group
    {
        public Group()
        {
            MembersInGroups = new HashSet<MembersInGroups>();
        }

        public int Id { get; set; }
        public int GroupSetId { get; set; }

        public GroupSet GroupSet { get; set; }
        public ICollection<MembersInGroups> MembersInGroups { get; set; }


        //public void AddMember(Person newPerson)
        //{
        //    members.Add(newPerson);
        //}

        //public void RemoveMember(Person PersonToBeRemoved)
        //{
        //    members.Remove(PersonToBeRemoved);
        //}

        //public Person[] MembersArray
        //{
        //    get
        //    {
        //        return members.ToArray();
        //    }
        //}

        //public List<Person> Members
        //{
        //    get
        //    {
        //        return members;
        //    }
        //    set
        //    {
        //        members = value;
        //    }
        //}

        //public int[] MemberIdArray
        //{
        //    get
        //    {
        //        int[] idArray = new int[members.Count];
        //        for (int i = 0; i < members.Count; i++)
        //        {
        //            idArray[i] = members[i].Id;
        //        }
        //        return idArray;
        //    }
        //}

        //public int NumberOfMembers
        //{
        //    get
        //    {
        //        return members.Count;
        //    }
        //}

        //public int WorkedTogetherPoints
        //{
        //    get
        //    {
        //        int points = 0;
        //        for (int i = 0; i < members.Count; i++)
        //        {
        //            for (int j = 0; j < i; j++)
        //            {

        //            }
        //        }
        //        return points;
        //    }
        //}
    }
}
