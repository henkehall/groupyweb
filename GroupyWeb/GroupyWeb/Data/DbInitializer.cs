﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using GroupyWeb.Models;

namespace GroupyWeb.Data
{
    public class DbInitializer
    {
        public static void Initialize(GroupyContext context)
        {
            context.Database.EnsureCreated();

            // Look for any Persons.
            if (context.Persons.Any())
            {
                return;   // DB has been seeded
            }

            var setting = new Setting();
            context.Settings.Add(setting);
            context.SaveChanges();

            var persons = new Person[]
            {
                new Person { Name= "Carson Alexander" },
                new Person { Name= "Meredith Alonso" },
                new Person { Name = "Arturo Anand" },
                new Person { Name = "Gytis Barzdukas" },
                new Person { Name = "Yan Li" },
                new Person { Name = "Peggy Justice" },
                new Person { Name = "Laura Norman" },
                new Person { Name = "Nino Olivetto" },
            };
            foreach (Person p in persons)
            {
                setting.Persons.Add(p);
            }
            context.SaveChanges();

            //MakeGroupsAndStuff(context, setting, persons);

        }

        private static void MakeGroupsAndStuff(GroupyContext context, Setting setting, Person[] persons)
        {

            List<PersonRelation> relations = new List<PersonRelation>();

            for (int i = 0; i < persons.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    relations.Add(new PersonRelation(persons[i], persons[j]));
                    context.Personrelations.Add(relations[relations.Count - 1]);
                }
            }
            context.SaveChanges();

            GroupSet groupset1 = new GroupSet();
            //groupset1.RandomizeGroupByNumber(relations, persons, 2);
            setting.Groupsets.Add(groupset1);
            context.SaveChanges();
        }
    }
}