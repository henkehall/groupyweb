﻿using GroupyWeb.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroupyWeb.Data
{
    public class GroupyContext : IdentityDbContext<GroupyUser>
    {
        public GroupyContext(DbContextOptions<GroupyContext> options) : base(options)
        {
        }

        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupSet> Groupsets { get; set; }
        public virtual DbSet<MembersInGroups> MembersInGroups { get; set; }
        public virtual DbSet<PersonRelation> Personrelations { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//            //optionsBuilder.UseSqlServer(@"Server=localhost\\SQLEXPRESS;Database=Groupy2;Trusted_Connection=True;");
//            optionsBuilder.UseSqlServer(@"Server=LIQUID\SQLEXPRESS;Database=Groupy2;Trusted_Connection=True;");
//            //optionsBuilder.UseSqlServer(@"Server=(local); Initial Catalog = Groupy2; Integrated Security = true;");
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Group>(entity =>
            {
                entity.HasOne(d => d.GroupSet)
                    .WithMany(p => p.Groups)
                    .HasForeignKey(d => d.GroupSetId);
            });

            modelBuilder.Entity<GroupSet>(entity =>
            {
                entity.HasOne(d => d.Setting)
                    .WithMany(p => p.Groupsets)
                    .HasForeignKey(d => d.SettingId);
            });

            modelBuilder.Entity<MembersInGroups>(entity =>
            {
                entity.HasKey(e => new { e.GroupId, e.MemberId });
                    //.ForSqlServerIsClustered(true);
                //entity.HasKey(e => new { e.GroupId, e.MemberId })
                //    .ForSqlServerIsClustered(false);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.MembersInGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MembersInGroups)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PersonRelation>(entity =>
            {
                entity.HasKey(e => new { e.P1id, e.P2id })
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.P1id).HasColumnName("P1Id");

                entity.Property(e => e.P2id).HasColumnName("P2Id");

                entity.HasOne(d => d.P1)
                    .WithMany(p => p.PersonRelationsP1)
                    .HasForeignKey(d => d.P1id)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.P2)
                    .WithMany(p => p.PersonRelationsP2)
                    .HasForeignKey(d => d.P2id)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.Setting)
                    .WithMany(p => p.Persons)
                    .HasForeignKey(d => d.SettingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.HasOne(d => d.GroupyUser)
                    .WithMany(p => p.Settings)
                    .HasForeignKey(d => d.UserId);
            });
        }
    }
}
