﻿//// Make Persons draggable
//$(function () {
//    $(".connectedSortable").sortable({
//        connectWith: ".connectedSortable"
//    }).disableSelection();
//});


// press enter to add person
$('#Name').keypress(function (event) {
    if (event.keyCode === 13) {
        AddPerson();
    }
});

//Add Person Function
function AddPerson() {
    var res = validate();
    if (res === false) {
        return false;
    }
    var persObj = {
        Name: $('#Name').val(),
        SettingId: $('#SettingId').val()
    };
    $.ajax({
        url: "/people/Add",
        data: JSON.stringify(persObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#personListSection .personlist').append('<div class="person col-md-3">' + data.id + '. ' + data.name + '<input type="checkbox" name="Active" value="Active" id="Active' + data.id + '" checked onchange="ToggleActive(' + data.id +')"></div>');
            $('#Name').val("");
        },
        failure: function (response) {
            alert("failure: "+errormessage.responseText);
        },
        error: function (errormessage) {
            alert("error: " +errormessage.responseText);
        }
    });
}

// validate add person form
function validate() {
    var isValid = true;
    if ($('#Name').val().trim() === "") {
        $('#Name').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Name').css('border-color', 'lightgrey');
    }
    return isValid;
}

//Add Person Function
function ToggleActive(id) {
    var persObj = {
        Id: id,
        Active: $('#Active'+id).is(':checked')
    };
    $.ajax({
        url: "/people/ToggleActive",
        data: JSON.stringify(persObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.active) {
                $('#Active' + id).parent().removeClass('inactive')
            } else {
                $('#Active' + id).parent().addClass('inactive')
            }
        },
        failure: function (response) {
            alert("failure: " + errormessage.responseText);
        },
        error: function (errormessage) {
            alert("error: " + errormessage.responseText);
        }
    });
}

// automatically assign people to groups
function assignToGroups(){
    var groupAssignment = {
        Id: $('#SettingId').val(),
        GroupCount: 5
    };
    $.ajax({
        url: "/GroupMaker/AssignToGroups",
        data: JSON.stringify(groupAssignment),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        //dataType: "json",
        dataType: "html",
        //success: function (result) {
        success: function (data) {
            //alert("sucess!! " + data);
            //alert(data);
            $('#groupSetSection').html(data);
        },
        failure: function (response) {
            alert("fail: " + response);
            alert("failure: " + errormessage.responseText);
        },
        error: function (errormessage) {
            alert("err: " + response);
            alert("error: " + errormessage.responseText);
        }
    });
}