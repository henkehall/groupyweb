﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GroupyWeb.Data;
using GroupyWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace GroupyWeb.Controllers
{
    [Authorize]
    public class GroupMakerController : Controller
    {
        private readonly GroupyContext _context;

        public GroupMakerController(GroupyContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            //var setting = await _context.Settings.SingleOrDefaultAsync(m => m.Id == id).;
            var setting = await _context.Settings.Include(m => m.Persons)
                                                 .Include(m => m.Groupsets).ThenInclude(gs => gs.Groups)
                                                 .SingleOrDefaultAsync(m => m.Id == id);
            //var setting = _context.Settings.Where(m => m.Id == id).Include(m => m.Persons).Include(m => m.Groupsets).ThenInclude(g => g.Groups).FirstOrDefault();
            if (setting == null)
            {
                return NotFound();
            }

            ViewData["Setting"] = setting;
            ViewData["Persons"] = setting.Persons.ToList();
            ViewData["GroupSets"] = setting.Groupsets.ToList();
            var groups = new List<Group>();
            var membersInGroups = new List<MembersInGroups>();
            foreach (var groupset in setting.Groupsets) //get all groups in setting
            {
                foreach (var group in groupset.Groups)
                {
                    groups.Add(group);
                    foreach (var memInGr in _context.MembersInGroups.Where(m => m.Group == group))
                        //get all member connections to this group
                    {
                        membersInGroups.Add(memInGr);
                    }
                }
            }
            ViewData["Groups"] = groups;
            ViewData["MembersInGroups"] = membersInGroups;
            return View();
        }

        [HttpPost]
        //async Task<IActionResult>
        public async Task<IActionResult> AssignToGroups([FromBody]jsonInputClass input)
        {
            //todo - do some check to see if setting id is valid and stuff

            Setting setting = await _context.Settings
                .Include(s => s.Persons).ThenInclude(p => p.PersonRelationsP1)
                .Include(s => s.Groupsets).ThenInclude(gr => gr.Groups).ThenInclude(g => g.MembersInGroups)
                //.Include(m => m.Persons).ThenInclude(p => p.PersonRelationsP2)
                .SingleOrDefaultAsync(m => m.Id == input.Id);
            //GroupSet newGroupSet = new GroupSet();
            //setting.Groupsets.Add(newGroupSet);
            Person[] activePeople = setting.Persons.Where(p => p.Active == true).ToArray();

            List<PersonRelation> relations = new List<PersonRelation>();
            foreach (var person in activePeople)
            {
                foreach (var relation in person.PersonRelationsP1)
                {
                    relations.Add(relation);
                }
            }
            GroupSet groupSet = GroupMaker.AssignToGroups(activePeople, relations, 5, out var membersInGroups);
            foreach (var memberInGroup in membersInGroups)
            {
                _context.Add(memberInGroup);
            }
            setting.Groupsets.Add(groupSet);
            //newGroupSet.RandomizeGroupByNumber(relations, setting.Persons.ToArray(), input.GroupCount);
            await _context.SaveChangesAsync();
            //var setting = _context.Settings.SingleOrDefault(m => m.Id == p.SettingId);
            //setting.Persons.Add(p);
            //_context.SaveChanges();
            //return Json(new { Id = p.Id, Name = p.Name });

            ViewData["Setting"] = setting;
            ViewData["GroupSets"] = setting.Groupsets.ToList();
            var groups = new List<Group>();
            var membersInAllGroups = new List<MembersInGroups>();
            foreach (var groupset in setting.Groupsets) //get all groups in setting
            {
                groups.AddRange(groupSet.Groups);
                foreach (var group in groupset.Groups)
                {
                    foreach (var memInGr in _context.MembersInGroups.Where(m => m.Group == group))
                    //get all member connections to this group
                    {
                        membersInAllGroups.Add(memInGr);
                    }
                }
            }
            //ViewData["Groups"] = groups;
            ViewData["MembersInGroups"] = membersInAllGroups;
            //ViewData["Persons"] = activePeople;
            return PartialView("_GroupSetList");
            //return Json("");
        }
    }
}