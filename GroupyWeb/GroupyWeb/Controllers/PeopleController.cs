﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GroupyWeb.Data;
using GroupyWeb.Models;

namespace GroupyWeb.Controllers
{
    public class PeopleController : Controller
    {
        private readonly GroupyContext _context;

        public PeopleController(GroupyContext context)
        {
            _context = context;
        }

        // GET: People
        public async Task<IActionResult> Index()
        {
            var groupyContext = _context.Persons.Include(p => p.Setting);
            return View(await groupyContext.ToListAsync());
        }

        // GET: People/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Persons
                .Include(p => p.Setting)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody][Bind(include:"Name,SettingId")]Person p)
        {
            //todo - do some check to see if settings id is valid and stuff
            var setting = await _context.Settings.SingleOrDefaultAsync(m => m.Id == p.SettingId);
            setting.Persons.Add(p);
            _context.SaveChanges();
            return Json(new { Id = p.Id, Name = p.Name });
        }

        [HttpPost]
        public async Task<IActionResult> ToggleActive([FromBody][Bind(include: "Id,Active")]Person p)
        {
            //todo - do some check to see if id is valid and stuff
            Person personToUpdate = await _context.Persons.SingleOrDefaultAsync(per => per.Id == p.Id);

            personToUpdate.Active = p.Active;
            _context.SaveChanges();
            return Json(new { Id = p.Id, Active = p.Active });
        }

        // GET: People/Create
        public IActionResult Create()
        {
            ViewData["SettingId"] = new SelectList(_context.Settings, "Id", "Id");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,Active,Name,SettingId")] Person person)
        public async Task<IActionResult> Create(Person person)
        {
            if (ModelState.IsValid)
            {
                _context.Add(person);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SettingId"] = new SelectList(_context.Settings, "Id", "Id", person.SettingId);
            return View(person);
        }

        // GET: People/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Persons.SingleOrDefaultAsync(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            ViewData["SettingId"] = new SelectList(_context.Settings, "Id", "Id", person.SettingId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Active,Name,SettingId")] Person person)
        {
            if (id != person.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(person);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonExists(person.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SettingId"] = new SelectList(_context.Settings, "Id", "Id", person.SettingId);
            return View(person);
        }

        // GET: People/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Persons
                .Include(p => p.Setting)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var person = await _context.Persons.SingleOrDefaultAsync(m => m.Id == id);
            _context.Persons.Remove(person);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonExists(int id)
        {
            return _context.Persons.Any(e => e.Id == id);
        }
    }
}
