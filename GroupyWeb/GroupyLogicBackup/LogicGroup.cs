﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class LogicGroup
    {
        private List<LogicPerson> members = new List<LogicPerson>();

        public void AddMember(LogicPerson newPerson)
        {
            members.Add(newPerson);
        }

        public void RemoveMember(LogicPerson PersonToBeRemoved)
        {
            members.Remove(PersonToBeRemoved);
        }

        public LogicPerson[] MembersArray
        {
            get
            {
                return members.ToArray();
            }
        }

        public List<LogicPerson> Members
        {
            get
            {
                return members;
            }
            set
            {
                members = value;
            }
        }

        public int[] MemberIdArray
        {
            get
            {
                int[] idArray = new int[members.Count];
                for (int i = 0; i < members.Count; i++)
                {
                    idArray[i] = members[i].Id;
                }
                return idArray;
            }
        }

        public int NumberOfMembers
        {
            get
            {
                return members.Count;
            }
        }

        public int WorkedTogetherPoints
        {
            get
            {
                int points = 0;
                for (int i = 0; i < members.Count; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        
                    }
                }
                return points;
            }
        }
    }
}
