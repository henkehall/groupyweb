﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class LogicRelation
    {
        int timesWorked;
        bool avoid;

        public LogicRelation()
        {
            timesWorked = 0;
            avoid = false;
        }

        public void AddTimesWorked()
        {
            timesWorked++;
        }

        public int TimesWorked
        {
            get
            {
                return timesWorked;
            }
            set
            {
                timesWorked = value;
            }
        }

        public bool Avoid
        {
            get
            {
                return avoid;
            }
            set
            {
                avoid = value;
            }
        }


    }
}