﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupyLogic
{
    public class LogicGroup
    {
        private List<int> members = new List<int>();

        public void AddMember(int newPerson)
        {
            members.Add(newPerson);
        }

        public void RemoveMember(int PersonToBeRemoved)
        {
            members.Remove(PersonToBeRemoved);
        }

        public int[] MembersArray
        {
            get
            {
                return members.ToArray();
            }
        }

        public List<int> Members
        {
            get
            {
                return members;
            }
            set
            {
                members = value;
            }
        }

        //public int[] MemberIdArray
        //{
        //    get
        //    {
        //        int[] idArray = new int[members.Count];
        //        for (int i = 0; i < members.Count; i++)
        //        {
        //            idArray[i] = members[i].Id;
        //        }
        //        return idArray;
        //    }
        //}

        public int NumberOfMembers
        {
            get
            {
                return members.Count;
            }
        }

        //public int WorkedTogetherPoints
        //{
        //    get
        //    {
        //        int points = 0;
        //        for (int i = 0; i < members.Count; i++)
        //        {
        //            for (int j = 0; j < i; j++)
        //            {
                        
        //            }
        //        }
        //        return points;
        //    }
        //}
    }
}
