# README #

### Groupy ###

* Automatically create groups from a set of people, subsequent groupsets will attempt to optimize so people will work with new people as much as possible.

### What works? ###

* Adding people (with direct connection to database with ajax)
* Toggling active/non-active
* Creating *one* groupset. Only one since relationdata is calculated but not saved to database
* Register/Login. But no real impact on app data at the moment

### Earlier versions ###

* https://bitbucket.org/henkehall/groupy